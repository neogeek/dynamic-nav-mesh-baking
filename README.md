# Dynamic Nav Mesh Baking

I'm trying to get a nav mesh to bake after a procedurally generated level is built. I'm using the new [NavMeshComponents](https://github.com/Unity-Technologies/NavMeshComponents/) project from GitHub. I've added the NavMeshSurface component to the parent object and both baked the nav mesh via the editor and added a script that bakes it at runtime. The resulting nav meshes are drastically different, as can be seen in the animation below.

![](https://media.giphy.com/media/l0HUlPJCADcAGJypO/giphy.gif)
